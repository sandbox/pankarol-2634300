# Starttr
Simple blog starter theme for Drupal 8

## About
Starttr is simple blog starter theme, what means, that you can completely modify
it. Comparing to other starter themes, starttr is just a couple of files like
normalize.css, basic styling, simple grid and responsive menu, so it is not necessary to use
this theme for a sub-theme.

## Instalation
Starttr is created to be your own theme rather than 'starttr', so to change the
name of theme follow this steps:

  - unpack zip file or clone repo to `PATH_TO_DRUPAL/sites/all/themes/`
  - rename the theme folder to your's theme name, ex. 'mytheme'
  - edit the `starttr.info.yml` file and change every 'starttr' word to your
    theme name
  - rename `starttr.info.yml`, ex. `mytheme.info.yml`
  - rename `starttr.libraries.yml`, ex. `mytheme.libraries.yml`

## Licenses
  - normalize.css - [github.com/necolas/normalize.css/](https://github.com/necolas/normalize.css/)
  - simplegrid.css - [github.com/ThisIsDallas/Simple-Grid](https://github.com/ThisIsDallas/Simple-Grid), under the MIT license.
  

  
